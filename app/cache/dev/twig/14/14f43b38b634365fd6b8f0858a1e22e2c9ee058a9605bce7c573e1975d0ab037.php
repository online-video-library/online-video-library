<?php

/* base.html.twig */
class __TwigTemplate_44658b362e30f5240975f67e8297b494542bd4ca59c972a9c75caadfee5953ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
            'add_categorie' => array($this, 'block_add_categorie'),
            'edit_categorie' => array($this, 'block_edit_categorie'),
            'show_categorie' => array($this, 'block_show_categorie'),
            'details_categorie' => array($this, 'block_details_categorie'),
            'add_film' => array($this, 'block_add_film'),
            'edit_film' => array($this, 'block_edit_film'),
            'show_film' => array($this, 'block_show_film'),
            'details_film' => array($this, 'block_details_film'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebfc9db5f8d06baf68ef3fff4dad07fa3ca3702b7ee4cb954834899c7e37db29 = $this->env->getExtension("native_profiler");
        $__internal_ebfc9db5f8d06baf68ef3fff4dad07fa3ca3702b7ee4cb954834899c7e37db29->enter($__internal_ebfc9db5f8d06baf68ef3fff4dad07fa3ca3702b7ee4cb954834899c7e37db29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "

<center>
<h1>Menu</h1>
</center> 


<ul>
          <li>
             <a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("add_film");
        echo "\"> Création des  nouveaux films </a> 
           </li>


           <li>
             <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("add_category");
        echo "\">Création des   catégories </a> 
           </li>


           <li>
             <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("show_category");
        echo "\"> Liste des catégories </a> 
           </li>

           <li>
             <a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("show_film");
        echo "\"> Liste des films </a> 
           </li>

          <li>
             <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("recherche_film");
        echo "\"> Recherche des films </a> 
           </li>


             ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("AppBundle:Film:counter"));
        echo "

             
</ul>


         ";
        // line 50
        $this->displayBlock('add_categorie', $context, $blocks);
        // line 51
        echo "          ";
        $this->displayBlock('edit_categorie', $context, $blocks);
        // line 52
        echo "         ";
        $this->displayBlock('show_categorie', $context, $blocks);
        // line 53
        echo "        ";
        $this->displayBlock('details_categorie', $context, $blocks);
        // line 54
        echo "




         ";
        // line 59
        $this->displayBlock('add_film', $context, $blocks);
        // line 60
        echo "          ";
        $this->displayBlock('edit_film', $context, $blocks);
        // line 61
        echo "         ";
        $this->displayBlock('show_film', $context, $blocks);
        // line 62
        echo "        ";
        $this->displayBlock('details_film', $context, $blocks);
        // line 63
        echo "



    </body>
</html>
";
        
        $__internal_ebfc9db5f8d06baf68ef3fff4dad07fa3ca3702b7ee4cb954834899c7e37db29->leave($__internal_ebfc9db5f8d06baf68ef3fff4dad07fa3ca3702b7ee4cb954834899c7e37db29_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fb54015fa2f751808f4aecab6dedf6f4568d7b1526f8ebe793e9fd8c2588e0ea = $this->env->getExtension("native_profiler");
        $__internal_fb54015fa2f751808f4aecab6dedf6f4568d7b1526f8ebe793e9fd8c2588e0ea->enter($__internal_fb54015fa2f751808f4aecab6dedf6f4568d7b1526f8ebe793e9fd8c2588e0ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_fb54015fa2f751808f4aecab6dedf6f4568d7b1526f8ebe793e9fd8c2588e0ea->leave($__internal_fb54015fa2f751808f4aecab6dedf6f4568d7b1526f8ebe793e9fd8c2588e0ea_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b50e7007ef4ad1d39f8a5507e00080eeb1b6566282e82336051c5a0cc5203718 = $this->env->getExtension("native_profiler");
        $__internal_b50e7007ef4ad1d39f8a5507e00080eeb1b6566282e82336051c5a0cc5203718->enter($__internal_b50e7007ef4ad1d39f8a5507e00080eeb1b6566282e82336051c5a0cc5203718_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_b50e7007ef4ad1d39f8a5507e00080eeb1b6566282e82336051c5a0cc5203718->leave($__internal_b50e7007ef4ad1d39f8a5507e00080eeb1b6566282e82336051c5a0cc5203718_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f92ac47fcecde2b73defd2e471664c4d9163204a76934e151fe7b1d91757b029 = $this->env->getExtension("native_profiler");
        $__internal_f92ac47fcecde2b73defd2e471664c4d9163204a76934e151fe7b1d91757b029->enter($__internal_f92ac47fcecde2b73defd2e471664c4d9163204a76934e151fe7b1d91757b029_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f92ac47fcecde2b73defd2e471664c4d9163204a76934e151fe7b1d91757b029->leave($__internal_f92ac47fcecde2b73defd2e471664c4d9163204a76934e151fe7b1d91757b029_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b52a0997ab436166f681a95c6247390b86771bfdb351ef58fa4067d736c5916d = $this->env->getExtension("native_profiler");
        $__internal_b52a0997ab436166f681a95c6247390b86771bfdb351ef58fa4067d736c5916d->enter($__internal_b52a0997ab436166f681a95c6247390b86771bfdb351ef58fa4067d736c5916d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_b52a0997ab436166f681a95c6247390b86771bfdb351ef58fa4067d736c5916d->leave($__internal_b52a0997ab436166f681a95c6247390b86771bfdb351ef58fa4067d736c5916d_prof);

    }

    // line 50
    public function block_add_categorie($context, array $blocks = array())
    {
        $__internal_3d9d79b57ccb763565cf50180c74844ae218b7c6d114fcde1796adfc1680737c = $this->env->getExtension("native_profiler");
        $__internal_3d9d79b57ccb763565cf50180c74844ae218b7c6d114fcde1796adfc1680737c->enter($__internal_3d9d79b57ccb763565cf50180c74844ae218b7c6d114fcde1796adfc1680737c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "add_categorie"));

        
        $__internal_3d9d79b57ccb763565cf50180c74844ae218b7c6d114fcde1796adfc1680737c->leave($__internal_3d9d79b57ccb763565cf50180c74844ae218b7c6d114fcde1796adfc1680737c_prof);

    }

    // line 51
    public function block_edit_categorie($context, array $blocks = array())
    {
        $__internal_d22ea8f569c820ea684c51b5e0c0f315b2196337897693931ccce72e8021cd53 = $this->env->getExtension("native_profiler");
        $__internal_d22ea8f569c820ea684c51b5e0c0f315b2196337897693931ccce72e8021cd53->enter($__internal_d22ea8f569c820ea684c51b5e0c0f315b2196337897693931ccce72e8021cd53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "edit_categorie"));

        
        $__internal_d22ea8f569c820ea684c51b5e0c0f315b2196337897693931ccce72e8021cd53->leave($__internal_d22ea8f569c820ea684c51b5e0c0f315b2196337897693931ccce72e8021cd53_prof);

    }

    // line 52
    public function block_show_categorie($context, array $blocks = array())
    {
        $__internal_d1a16f4f7a4c1cac4f57a9abaab3314c5d01a8428bcc243c43d4aa9d308ca980 = $this->env->getExtension("native_profiler");
        $__internal_d1a16f4f7a4c1cac4f57a9abaab3314c5d01a8428bcc243c43d4aa9d308ca980->enter($__internal_d1a16f4f7a4c1cac4f57a9abaab3314c5d01a8428bcc243c43d4aa9d308ca980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_categorie"));

        
        $__internal_d1a16f4f7a4c1cac4f57a9abaab3314c5d01a8428bcc243c43d4aa9d308ca980->leave($__internal_d1a16f4f7a4c1cac4f57a9abaab3314c5d01a8428bcc243c43d4aa9d308ca980_prof);

    }

    // line 53
    public function block_details_categorie($context, array $blocks = array())
    {
        $__internal_50335398692be164eaecab0f1119c4625cf6bd8b43749827175b2134d6398acc = $this->env->getExtension("native_profiler");
        $__internal_50335398692be164eaecab0f1119c4625cf6bd8b43749827175b2134d6398acc->enter($__internal_50335398692be164eaecab0f1119c4625cf6bd8b43749827175b2134d6398acc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "details_categorie"));

        
        $__internal_50335398692be164eaecab0f1119c4625cf6bd8b43749827175b2134d6398acc->leave($__internal_50335398692be164eaecab0f1119c4625cf6bd8b43749827175b2134d6398acc_prof);

    }

    // line 59
    public function block_add_film($context, array $blocks = array())
    {
        $__internal_0cc7626b348256cc875485a4d449c2bdef9af6f64c32da4e6a3df96ff08f990e = $this->env->getExtension("native_profiler");
        $__internal_0cc7626b348256cc875485a4d449c2bdef9af6f64c32da4e6a3df96ff08f990e->enter($__internal_0cc7626b348256cc875485a4d449c2bdef9af6f64c32da4e6a3df96ff08f990e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "add_film"));

        
        $__internal_0cc7626b348256cc875485a4d449c2bdef9af6f64c32da4e6a3df96ff08f990e->leave($__internal_0cc7626b348256cc875485a4d449c2bdef9af6f64c32da4e6a3df96ff08f990e_prof);

    }

    // line 60
    public function block_edit_film($context, array $blocks = array())
    {
        $__internal_8c2f3175b317dc1ded8e5097018dfa797c936e0623f589d19bf1dfeeab6c082b = $this->env->getExtension("native_profiler");
        $__internal_8c2f3175b317dc1ded8e5097018dfa797c936e0623f589d19bf1dfeeab6c082b->enter($__internal_8c2f3175b317dc1ded8e5097018dfa797c936e0623f589d19bf1dfeeab6c082b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "edit_film"));

        
        $__internal_8c2f3175b317dc1ded8e5097018dfa797c936e0623f589d19bf1dfeeab6c082b->leave($__internal_8c2f3175b317dc1ded8e5097018dfa797c936e0623f589d19bf1dfeeab6c082b_prof);

    }

    // line 61
    public function block_show_film($context, array $blocks = array())
    {
        $__internal_db19fc12a41cfdebd1f61faa0a19c424ede9802c4d63ed3f64bbdc619fc27167 = $this->env->getExtension("native_profiler");
        $__internal_db19fc12a41cfdebd1f61faa0a19c424ede9802c4d63ed3f64bbdc619fc27167->enter($__internal_db19fc12a41cfdebd1f61faa0a19c424ede9802c4d63ed3f64bbdc619fc27167_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_film"));

        
        $__internal_db19fc12a41cfdebd1f61faa0a19c424ede9802c4d63ed3f64bbdc619fc27167->leave($__internal_db19fc12a41cfdebd1f61faa0a19c424ede9802c4d63ed3f64bbdc619fc27167_prof);

    }

    // line 62
    public function block_details_film($context, array $blocks = array())
    {
        $__internal_1cfc96edfa64c5420998bf7ee7ccf587f62e11f36f7f4e4be27b26b42c39f9f3 = $this->env->getExtension("native_profiler");
        $__internal_1cfc96edfa64c5420998bf7ee7ccf587f62e11f36f7f4e4be27b26b42c39f9f3->enter($__internal_1cfc96edfa64c5420998bf7ee7ccf587f62e11f36f7f4e4be27b26b42c39f9f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "details_film"));

        
        $__internal_1cfc96edfa64c5420998bf7ee7ccf587f62e11f36f7f4e4be27b26b42c39f9f3->leave($__internal_1cfc96edfa64c5420998bf7ee7ccf587f62e11f36f7f4e4be27b26b42c39f9f3_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 62,  269 => 61,  258 => 60,  247 => 59,  236 => 53,  225 => 52,  214 => 51,  203 => 50,  192 => 12,  181 => 11,  170 => 6,  158 => 5,  145 => 63,  142 => 62,  139 => 61,  136 => 60,  134 => 59,  127 => 54,  124 => 53,  121 => 52,  118 => 51,  116 => 50,  107 => 44,  100 => 40,  93 => 36,  86 => 32,  78 => 27,  70 => 22,  59 => 13,  56 => 12,  54 => 11,  46 => 7,  44 => 6,  40 => 5,  34 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/* */
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/* */
/* */
/* <center>*/
/* <h1>Menu</h1>*/
/* </center> */
/* */
/* */
/* <ul>*/
/*           <li>*/
/*              <a href="{{ path('add_film') }}"> Création des  nouveaux films </a> */
/*            </li>*/
/* */
/* */
/*            <li>*/
/*              <a href="{{ path('add_category') }}">Création des   catégories </a> */
/*            </li>*/
/* */
/* */
/*            <li>*/
/*              <a href="{{ path('show_category') }}"> Liste des catégories </a> */
/*            </li>*/
/* */
/*            <li>*/
/*              <a href="{{ path('show_film') }}"> Liste des films </a> */
/*            </li>*/
/* */
/*           <li>*/
/*              <a href="{{ path('recherche_film') }}"> Recherche des films </a> */
/*            </li>*/
/* */
/* */
/*              {{ render(controller('AppBundle:Film:counter')) }}*/
/* */
/*              */
/* </ul>*/
/* */
/* */
/*          {% block add_categorie %}{% endblock %}*/
/*           {% block edit_categorie %}{% endblock %}*/
/*          {% block show_categorie %}{% endblock %}*/
/*         {% block details_categorie %}{% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/*          {% block add_film %}{% endblock %}*/
/*           {% block edit_film %}{% endblock %}*/
/*          {% block show_film %}{% endblock %}*/
/*         {% block details_film %}{% endblock %}*/
/* */
/* */
/* */
/* */
/*     </body>*/
/* </html>*/
/* */
