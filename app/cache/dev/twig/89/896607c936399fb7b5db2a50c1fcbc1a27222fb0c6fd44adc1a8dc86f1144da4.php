<?php

/* AppBundle:Film:show.html.twig */
class __TwigTemplate_1da44d25818317ca4efffe48c8aca2e2ce3e8826ed965307abeb048802b674c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Film:show.html.twig", 1);
        $this->blocks = array(
            'show_film' => array($this, 'block_show_film'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a0cc4eec88ac6a75f26d298a39aa81b1fa4eedaf20d884a6a6b86ceeffbdc3e = $this->env->getExtension("native_profiler");
        $__internal_2a0cc4eec88ac6a75f26d298a39aa81b1fa4eedaf20d884a6a6b86ceeffbdc3e->enter($__internal_2a0cc4eec88ac6a75f26d298a39aa81b1fa4eedaf20d884a6a6b86ceeffbdc3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Film:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a0cc4eec88ac6a75f26d298a39aa81b1fa4eedaf20d884a6a6b86ceeffbdc3e->leave($__internal_2a0cc4eec88ac6a75f26d298a39aa81b1fa4eedaf20d884a6a6b86ceeffbdc3e_prof);

    }

    // line 4
    public function block_show_film($context, array $blocks = array())
    {
        $__internal_88f57644f83756ced9aa679d1fbca5251a0a39a12b2019aaf2a64b167814e2a5 = $this->env->getExtension("native_profiler");
        $__internal_88f57644f83756ced9aa679d1fbca5251a0a39a12b2019aaf2a64b167814e2a5->enter($__internal_88f57644f83756ced9aa679d1fbca5251a0a39a12b2019aaf2a64b167814e2a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_film"));

        // line 5
        echo "<center>
    <h1>Liste  des films </h1>
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "            ";
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "
    <table>
        ";
        // line 12
        if ((twig_length_filter($this->env, (isset($context["films"]) ? $context["films"] : $this->getContext($context, "films"))) < 1)) {
            echo "    pas de film trouvé    ";
        }
        // line 13
        echo "               

        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
            // line 16
            echo "            <tr>     
                <td> Titre du film: </td>
                <td> ";
            // line 18
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($context["film"], "titre", array())), "html", null, true);
            echo "  </td>
            </tr>


            <tr>     
                <td> Description du film: </td>
                <td> ";
            // line 24
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($context["film"], "description", array())), "html", null, true);
            echo "  </td>
            </tr>


            <tr>     
                <td> Catégorie du film: </td>
                <td> ";
            // line 30
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute($context["film"], "category", array()), "titre", array())), "html", null, true);
            echo "  </td>
            </tr>

           <tr>     
                
                <td>  <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("update_film", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\"> modification du film </a>   </td>
                
                
                <td>  <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete_film", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\"> suppression du film </a>   </td>
                
                
                <td>  <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("show_film_details", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\"> Détail du film </a>   </td>
            </tr>


        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "
    </table> 

</center>


";
        
        $__internal_88f57644f83756ced9aa679d1fbca5251a0a39a12b2019aaf2a64b167814e2a5->leave($__internal_88f57644f83756ced9aa679d1fbca5251a0a39a12b2019aaf2a64b167814e2a5_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Film:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 46,  115 => 41,  109 => 38,  103 => 35,  95 => 30,  86 => 24,  77 => 18,  73 => 16,  69 => 15,  65 => 13,  61 => 12,  57 => 10,  48 => 8,  44 => 7,  40 => 5,  34 => 4,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block show_film %}*/
/* <center>*/
/*     <h1>Liste  des films </h1>*/
/*         {% for flash_message in app.session.flashbag.get('success') %}*/
/*             {{ flash_message }}*/
/*         {% endfor %}*/
/* */
/*     <table>*/
/*         {% if  films|length < 1  %}    pas de film trouvé    {%endif%}*/
/*                */
/* */
/*         {% for film   in films %}*/
/*             <tr>     */
/*                 <td> Titre du film: </td>*/
/*                 <td> {{  film.titre|capitalize }}  </td>*/
/*             </tr>*/
/* */
/* */
/*             <tr>     */
/*                 <td> Description du film: </td>*/
/*                 <td> {{  film.description|capitalize }}  </td>*/
/*             </tr>*/
/* */
/* */
/*             <tr>     */
/*                 <td> Catégorie du film: </td>*/
/*                 <td> {{  film.category.titre |capitalize}}  </td>*/
/*             </tr>*/
/* */
/*            <tr>     */
/*                 */
/*                 <td>  <a href="{{ path('update_film',{'id':film.id}) }}"> modification du film </a>   </td>*/
/*                 */
/*                 */
/*                 <td>  <a href="{{ path('delete_film',{'id':film.id}) }}"> suppression du film </a>   </td>*/
/*                 */
/*                 */
/*                 <td>  <a href="{{ path('show_film_details',{'id':film.id}) }}"> Détail du film </a>   </td>*/
/*             </tr>*/
/* */
/* */
/*         {% endfor %}*/
/* */
/*     </table> */
/* */
/* </center>*/
/* */
/* */
/* {% endblock %}*/
