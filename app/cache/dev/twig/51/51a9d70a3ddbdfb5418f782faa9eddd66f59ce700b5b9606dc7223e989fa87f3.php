<?php

/* mails/suppression_film_notification.html.twig */
class __TwigTemplate_304ec6199ec0b38b7be83e166d0e9a00b600faeef2ac01f47f8b2f820c8ab815 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0824b1086538d157a379f973c9a2c4ae1e58ad8211bb4df0f8e6e61274954f5 = $this->env->getExtension("native_profiler");
        $__internal_a0824b1086538d157a379f973c9a2c4ae1e58ad8211bb4df0f8e6e61274954f5->enter($__internal_a0824b1086538d157a379f973c9a2c4ae1e58ad8211bb4df0f8e6e61274954f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "mails/suppression_film_notification.html.twig"));

        // line 1
        echo "

Un   film  avec titre ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")), "titre", array()), "html", null, true);
        echo "  a était supprimé.";
        
        $__internal_a0824b1086538d157a379f973c9a2c4ae1e58ad8211bb4df0f8e6e61274954f5->leave($__internal_a0824b1086538d157a379f973c9a2c4ae1e58ad8211bb4df0f8e6e61274954f5_prof);

    }

    public function getTemplateName()
    {
        return "mails/suppression_film_notification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 1,);
    }
}
/* */
/* */
/* Un   film  avec titre {{film.titre}}  a était supprimé.*/
