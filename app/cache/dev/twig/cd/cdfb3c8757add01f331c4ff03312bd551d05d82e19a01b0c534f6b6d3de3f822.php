<?php

/* mails/ajout_film_notification.html.twig */
class __TwigTemplate_1b0a1b15c5d551b3ce63b7f8928de5d023aac009b451e97fd51b305b80c1de77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fa555613ac977d640f45bf7b8db147b54b6d40fa266967ed6e21a57f397a6ed = $this->env->getExtension("native_profiler");
        $__internal_6fa555613ac977d640f45bf7b8db147b54b6d40fa266967ed6e21a57f397a6ed->enter($__internal_6fa555613ac977d640f45bf7b8db147b54b6d40fa266967ed6e21a57f397a6ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "mails/ajout_film_notification.html.twig"));

        // line 1
        echo "


Un nouveau film avec le titre ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")), "titre", array()), "html", null, true);
        echo "  est créé.

";
        
        $__internal_6fa555613ac977d640f45bf7b8db147b54b6d40fa266967ed6e21a57f397a6ed->leave($__internal_6fa555613ac977d640f45bf7b8db147b54b6d40fa266967ed6e21a57f397a6ed_prof);

    }

    public function getTemplateName()
    {
        return "mails/ajout_film_notification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* */
/* */
/* */
/* Un nouveau film avec le titre {{film.titre}}  est créé.*/
/* */
/* */
