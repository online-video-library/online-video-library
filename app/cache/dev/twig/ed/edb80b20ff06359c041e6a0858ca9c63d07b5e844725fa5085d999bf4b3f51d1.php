<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0a27152efac08d1cdfa13f421656621ceefb7c1f7475028a3094b33586ad4940 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c402b48fe2d5078a8f62a99fa81742580e0b79a94b9ca39fc389455d21efbd7b = $this->env->getExtension("native_profiler");
        $__internal_c402b48fe2d5078a8f62a99fa81742580e0b79a94b9ca39fc389455d21efbd7b->enter($__internal_c402b48fe2d5078a8f62a99fa81742580e0b79a94b9ca39fc389455d21efbd7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c402b48fe2d5078a8f62a99fa81742580e0b79a94b9ca39fc389455d21efbd7b->leave($__internal_c402b48fe2d5078a8f62a99fa81742580e0b79a94b9ca39fc389455d21efbd7b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b63538ebbff0297c488468d004279c117fb14f0fef795128d1448c62f343b2f3 = $this->env->getExtension("native_profiler");
        $__internal_b63538ebbff0297c488468d004279c117fb14f0fef795128d1448c62f343b2f3->enter($__internal_b63538ebbff0297c488468d004279c117fb14f0fef795128d1448c62f343b2f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b63538ebbff0297c488468d004279c117fb14f0fef795128d1448c62f343b2f3->leave($__internal_b63538ebbff0297c488468d004279c117fb14f0fef795128d1448c62f343b2f3_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c228a6676654f78cff8e6007ad33a495c3625952002f6c24395b1d739873e8ba = $this->env->getExtension("native_profiler");
        $__internal_c228a6676654f78cff8e6007ad33a495c3625952002f6c24395b1d739873e8ba->enter($__internal_c228a6676654f78cff8e6007ad33a495c3625952002f6c24395b1d739873e8ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_c228a6676654f78cff8e6007ad33a495c3625952002f6c24395b1d739873e8ba->leave($__internal_c228a6676654f78cff8e6007ad33a495c3625952002f6c24395b1d739873e8ba_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a3feb9cf51967e262d29c13884f1a95dea59232dbfcdbc2835fe5253e49aedcd = $this->env->getExtension("native_profiler");
        $__internal_a3feb9cf51967e262d29c13884f1a95dea59232dbfcdbc2835fe5253e49aedcd->enter($__internal_a3feb9cf51967e262d29c13884f1a95dea59232dbfcdbc2835fe5253e49aedcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a3feb9cf51967e262d29c13884f1a95dea59232dbfcdbc2835fe5253e49aedcd->leave($__internal_a3feb9cf51967e262d29c13884f1a95dea59232dbfcdbc2835fe5253e49aedcd_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
