<?php

/* AppBundle:Film:add.html.twig */
class __TwigTemplate_5e46d8aecaa8a9c4d748b0f96f0a286db2d0d1da540aa49337c326548479a9fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Film:add.html.twig", 1);
        $this->blocks = array(
            'add_film' => array($this, 'block_add_film'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5dc218ffb1293cb90b0a7617c46390bf18c77913a7de5ec9b72d42657f257dda = $this->env->getExtension("native_profiler");
        $__internal_5dc218ffb1293cb90b0a7617c46390bf18c77913a7de5ec9b72d42657f257dda->enter($__internal_5dc218ffb1293cb90b0a7617c46390bf18c77913a7de5ec9b72d42657f257dda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Film:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5dc218ffb1293cb90b0a7617c46390bf18c77913a7de5ec9b72d42657f257dda->leave($__internal_5dc218ffb1293cb90b0a7617c46390bf18c77913a7de5ec9b72d42657f257dda_prof);

    }

    // line 3
    public function block_add_film($context, array $blocks = array())
    {
        $__internal_49cf78334fa0d9cf2e9492d8ce710d52ef62ebd09685f5fc0f4c723bcf93d76c = $this->env->getExtension("native_profiler");
        $__internal_49cf78334fa0d9cf2e9492d8ce710d52ef62ebd09685f5fc0f4c723bcf93d76c->enter($__internal_49cf78334fa0d9cf2e9492d8ce710d52ef62ebd09685f5fc0f4c723bcf93d76c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "add_film"));

        // line 4
        echo "<center>
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 6
            echo "        ";
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "

    <h1>Ajout d'un film </h1>

    ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontalndar", "novalidate" => "novalidate")));
        echo "
    <table>

        <tr>    
            <td>";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre", array()), 'label');
        echo " </td>
            <td>    ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre", array()), 'widget');
        echo " </td>
            <td>   ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre", array()), 'errors');
        echo " </td>
        </tr>

        <tr> 
            <td>    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'label');
        echo " </td>
            <td>  ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'widget');
        echo " </td>
            <td>  ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'errors');
        echo " </td>
        </tr>
        
          <tr> 
            <td>    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'label');
        echo " </td>
            <td>  ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo " </td>
            <td>  ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'errors');
        echo " </td>
        </tr>
        
        
        <tr> 
            <td> ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'label');
        echo " </td>
            <td>   ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'widget');
        echo " </td>
            <td>   ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "category", array()), 'errors');
        echo " </td>
        </tr>

    </table>

    <button type=\"reset\" class=\"btn btn-default\">reset</button>
    <button type=\"submit\" class=\"btn btn-info pull-right\">submit</button>


    ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "

    ";
        // line 48
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

</center>

";
        
        $__internal_49cf78334fa0d9cf2e9492d8ce710d52ef62ebd09685f5fc0f4c723bcf93d76c->leave($__internal_49cf78334fa0d9cf2e9492d8ce710d52ef62ebd09685f5fc0f4c723bcf93d76c_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Film:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 48,  135 => 46,  123 => 37,  119 => 36,  115 => 35,  107 => 30,  103 => 29,  99 => 28,  92 => 24,  88 => 23,  84 => 22,  77 => 18,  73 => 17,  69 => 16,  62 => 12,  56 => 8,  47 => 6,  43 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block add_film %}*/
/* <center>*/
/*     {% for flash_message in app.session.flashbag.get('success') %}*/
/*         {{ flash_message }}*/
/*     {% endfor %}*/
/* */
/* */
/*     <h1>Ajout d'un film </h1>*/
/* */
/*     {{ form_start(form, { 'attr': {'class': 'form-horizontalndar','novalidate': 'novalidate'} }) }}*/
/*     <table>*/
/* */
/*         <tr>    */
/*             <td>{{ form_label(form.titre) }} </td>*/
/*             <td>    {{ form_widget(form.titre) }} </td>*/
/*             <td>   {{ form_errors(form.titre) }} </td>*/
/*         </tr>*/
/* */
/*         <tr> */
/*             <td>    {{ form_label(form.description) }} </td>*/
/*             <td>  {{ form_widget(form.description) }} </td>*/
/*             <td>  {{ form_errors(form.description) }} </td>*/
/*         </tr>*/
/*         */
/*           <tr> */
/*             <td>    {{ form_label(form.file) }} </td>*/
/*             <td>  {{ form_widget(form.file) }} </td>*/
/*             <td>  {{ form_errors(form.file) }} </td>*/
/*         </tr>*/
/*         */
/*         */
/*         <tr> */
/*             <td> {{ form_label(form.category) }} </td>*/
/*             <td>   {{ form_widget(form.category) }} </td>*/
/*             <td>   {{ form_errors(form.category) }} </td>*/
/*         </tr>*/
/* */
/*     </table>*/
/* */
/*     <button type="reset" class="btn btn-default">reset</button>*/
/*     <button type="submit" class="btn btn-info pull-right">submit</button>*/
/* */
/* */
/*     {{ form_rest(form) }}*/
/* */
/*     {{ form_end(form) }}*/
/* */
/* </center>*/
/* */
/* {% endblock %}*/
