<?php

/* AppBundle:Counter:counter.html.twig */
class __TwigTemplate_9708613321f2041bfe92be7597819558bd3b230d2ebc144adfafdcc048354962 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2af974bfa08d1859cc3c6b301f4e3a9ca089b8be6d49da93a1f3e4ef1f222e49 = $this->env->getExtension("native_profiler");
        $__internal_2af974bfa08d1859cc3c6b301f4e3a9ca089b8be6d49da93a1f3e4ef1f222e49->enter($__internal_2af974bfa08d1859cc3c6b301f4e3a9ca089b8be6d49da93a1f3e4ef1f222e49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Counter:counter.html.twig"));

        // line 1
        echo "



Le nombre total des film est : ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["counter"]) ? $context["counter"] : $this->getContext($context, "counter")), "number", array()), "html", null, true);
        
        $__internal_2af974bfa08d1859cc3c6b301f4e3a9ca089b8be6d49da93a1f3e4ef1f222e49->leave($__internal_2af974bfa08d1859cc3c6b301f4e3a9ca089b8be6d49da93a1f3e4ef1f222e49_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Counter:counter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  22 => 1,);
    }
}
/* */
/* */
/* */
/* */
/* Le nombre total des film est : {{counter.number}}*/
