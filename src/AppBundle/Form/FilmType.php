<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('titre', TextType::class, array('required' => true,
                    'attr' => array(
                        'placeholder' => '',
                        'class' => 'form-control'
                    ),
                ))
                        ->add('file', 'file', array('required' => true ))

                ->add('description', TextareaType::class, array('required' => true,
                    'attr' => array(
                        'placeholder' => '',
                        'class' => 'form-control',
                        'height' => ' 110px;'
                )))
                ->add('category', 'entity', array('class' => 'AppBundle\Entity\Category',
                    'property' => 'titre',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                    'attr' => array('class' => 'form-control', 'height' => '34px'),
                ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Film',
        ));
    }

    public function getBlockPrefix()
    {
        return 'film';
    }
}
