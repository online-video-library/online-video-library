<?php


/**
 * @author Mehrez Labidi
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

 /**
 * counter.
 *
 * @ORM\Table(name="counter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CounterRepository")
 */


class Counter
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", length=25 ,nullable=true)
     */
    private $number;

    public function getId()
    {
        return $this->id;
    }
    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }
}
