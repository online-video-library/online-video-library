<?php

/**
 * @author Mehrez Labidi
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;

class CategoryController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $request = $this->get('request');
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $category = $form->getData();
                $em->persist($category);
                $em->flush();

                $this->addFlash('success', 'succès');
            }
        }

        return $this->render('AppBundle:Category:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);
        if (!$category) {
            throw $this->createNotFoundException('no category found');
        }
        $form = $this->createForm(CategoryType::class, $category);
        $request = $this->get('request');
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $category = $form->getData();
                $em->persist($category);
                $em->flush();
                $this->addFlash('success', 'succès');
            }
        }

        return $this->render('AppBundle:Category:update.html.twig', array('form' => $form->createView()));
    }

    public function showDetailsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);
        if (!$category) {
            throw $this->createNotFoundException('no category found');
        }

        return $this->render('AppBundle:Category:show_details.html.twig', array('category' => $category));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);
        if (!$category) {
            throw $this->createNotFoundException('no  categories found');
        }
        $em->remove($category);
        $em->flush();
        $this->addFlash('success', 'succès');
        return $this->redirect($this->generateUrl('show_category'));
    }

    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorys = $em->getRepository('AppBundle:Category')->findAll();
        return $this->render('AppBundle:Category:show.html.twig', array('categories' => $categorys));
    }
}
