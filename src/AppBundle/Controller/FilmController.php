<?php

/**
 * @author Mehrez Labidi
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Film;
use AppBundle\Entity\Counter;
use AppBundle\Form\FilmType;

class FilmController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $request =$this->get('request');
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $film = $form->getData();
                $em->persist($film);
                $em->flush();
                $this->addFlash('success', 'succès');
            }
        }

        return $this->render('AppBundle:Film:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('AppBundle:Film')->find($id);
        if (!$film) {
            throw $this->createNotFoundException('no film found');
        }
        $form = $this->createForm(FilmType::class, $film);
        $request = $this->get('request');
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $film = $form->getData();
                $em->persist($film);
                $em->flush();
                $this->addFlash('success', 'succès');
            }
        }

        return $this->render('AppBundle:Film:update.html.twig', array('form' => $form->createView()));
    }

    public function showDetailsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('AppBundle:Film')->find($id);
        if (!$film) {
            throw $this->createNotFoundException('no Film found');
        }

        return $this->render('AppBundle:Film:show_details.html.twig', array('film' => $film));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('AppBundle:Film')->find($id);
        if (!$film) {
            throw $this->createNotFoundException('no  Films found');
        }
        $em->remove($film);
        $em->flush();
        $this->addFlash('success', 'succès');
        return $this->redirect($this->generateUrl('show_film'));
    }

    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('AppBundle:Film')->findAll();
        return $this->render('AppBundle:Film:show.html.twig', array('films' => $films));
    }

    public function rechercheAction(Request $request)
    {
        $films =array();


      


        $form = $this->createFormBuilder()


            ->add('titre', 'text', array('required' => false ))
              ->add('date', 'text', array('required' => false ))
                ->add('category', 'entity', array('class' => 'AppBundle\Entity\Category',
                    'property' => 'titre',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                    'attr' => array('class' => 'form-control', 'height' => '34px'),
                ))
            ->getForm();
        
        $form->bind($request);
 
        
        if ($form->isValid()) {
            $data= $form->getData();
            foreach ($data as $key => $value) {
                if ($value) {
                    $arrayRecherche[$key] = $value;
                }
            }
 
            $em = $this->getDoctrine()->getManager();
            $films = $em->getRepository('AppBundle:Film')->findBy($arrayRecherche);
        }

       
 





        return $this->render('AppBundle:Film:recherche_film.html.twig', array('films' => $films,'form'=>$form->createView()));
    }




    public function showByCategoryAction($id, Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM AppBundle:Film a WHERE a.category ='$id'";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                10/*limit per page*/
            );
        return $this->render('AppBundle:Film:show_by_category.html.twig', array('pagination' => $pagination));
    }



    public function counterAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getManager();
        $numberFilms = $this->getDoctrine()
                            ->getRepository(Counter::class)
                            ->findLastOne();
                        
        return $this->render('AppBundle:Counter:counter.html.twig', array('counter' => $numberFilms));
    }
}
