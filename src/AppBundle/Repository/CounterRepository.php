<?php


namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CounterRepository extends EntityRepository
{
    public function findLastOne()
    {
        $qb =  $this->getEntityManager()->createQueryBuilder();
        $qb->select('a')
                ->from('AppBundle:Counter', 'a')
                 ->orderBy('a.id')
                ->setMaxResults(1);
        $query= $qb->getQuery();
        return $query->getResult()[0];
    }
}
