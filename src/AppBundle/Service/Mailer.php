<?php

namespace AppBundle\Service;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Swift_Message;

class Mailer
{
    private $mailer;
    private $templating;
   
    /*
       port: 465
        smtp: 'smtp.gmail.com'
        protocol: 'ssl'
        */
    public function senMail($view, $film, $from, $to, $subject, $port, $smtp, $protocol)
    {
        // contenu du mail
        $body = $this->templating->render($view, $film);
        $transport = \Swift_SmtpTransport::newInstance($port, $smtp, $protocol);
      
        $message = Swift_Message::newInstance($transport)
                ->setFrom($from)
                ->setTo($to)->addCc($from)
                ->setSubject($subject)
                ->setBody($body, 'text/html');
        $this->mailer->send($message);
    }
    /**
     * @param Swift_Mailer $mailer
     * @return void
     */
    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    /**
     * @param TwigEngine $templating
     * @return void
     */
    public function setTemplating(TwigEngine $templating)
    {
        $this->templating = $templating;
    }
}
