<?php

 
namespace AppBundle\DoctrineListener;

 
use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Counter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
 
use AppBundle\Entity\Film;
use AppBundle\Service\Manager;

class FilmCounterListener
{
    public function __construct($mailer, $port, $smtp, $protocol, $vue_ajout, $vue_suppression, $to, $from, $subject)
    {
        $this->mailer = $mailer;
        $this->port = $port;
        $this->smtp = $smtp;
        $this->protocol = $protocol;
        $this->vue_ajout = $vue_ajout;                       
        $this->vue_suppression = $vue_suppression;
        $this->to = $to;
        $this->from = $from;
        $this->subject = $subject;
    }
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ((!$entity instanceof Film)) {
            return;
        }

        if ($entity instanceof Film) {
            $counter = $em->getRepository('AppBundle:Counter')->find(1);
            $counter->setNumber((int)$counter->getNumber()+1);
            $em->persist($counter);
            $em->flush();

            $port= $this->port ;
            $smtp =   $this->smtp ;
            $protocol=   $this->protocol;
            $view =  $this->vue_ajout ;
            $film =  array('film'=>$entity );
            $to =  $this->to ;
            $from = $this->from ;
            $subject =   $this->subject ;
            $this->mailer ->senMail($view, $film, $from, $to, $subject, $port, $smtp, $protocol);
        }
    }


    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ((!$entity instanceof Film)) {
            return;
        }

        if ($entity instanceof Film) {
            $counter = $em->getRepository('AppBundle:Counter')->find(1);
            $counter->setNumber((int)$counter->getNumber()-1);
            $em->persist($counter);
            $em->flush();
           
            $port= $this->port ;
            $smtp = $this->smtp ;
            $protocol=   $this->protocol;
            $view =    $this->vue_suppression;
            $film =  array('film'=>$entity );
            $to =  $this->to ;
            $from =$this->from ;
            $subject  =  $this->subject ;
            $this->mailer ->senMail($view, $film, $from, $to, $subject, $port, $smtp, $protocol);
        }
    }
}
